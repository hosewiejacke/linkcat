// 2014-10-07

#include <err.h>
#include <fcntl.h>
#include <net/bpf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <unistd.h>
#include <errno.h>

#define PATH_SIZE (5 + IFNAMSIZ)	// strlen("/dev/) + IFNAMSIZ
#define FRAME_SIZE (14 + 1500)	    // 14B eth hdr + mtu

int main(int argc, char *argv[]) {
    int status = EXIT_FAILURE;	    // nur wenn alles OK auf EXIT_SUCCESS setzen

    // mindestens das interface muss angegeben werden
    if (argc != 2) {
        fprintf(stderr, "usage: %s <interface> <nibbles>\n", argv[0]);
        goto exit;
    }

    char *interface_name = argv[1];
    if (strlen(interface_name) >= IFNAMSIZ) {
        fprintf(stderr, "\"%s\" is to long\n", interface_name);
        goto exit;
    }

    // bpf suchen
    char bpf_path[PATH_SIZE];
    int bpf_index = 0;
    int bpf = -1;

    do { // do, um errno richtig zu setzen
        snprintf(bpf_path, PATH_SIZE, "/dev/bpf%i", bpf_index);
        bpf = open(bpf_path, O_WRONLY);
    }
    while (bpf < 0 && errno == EBUSY && ++bpf_index > 0);

    if(bpf < 0) {
        fprintf(stderr, "%s: %s\n", bpf_path, strerror(errno));
        goto exit;
    }

    // quell-mac nicht automatisch setzen
    unsigned nohdr = 1;
    if (ioctl(bpf, BIOCSHDRCMPLT, &nohdr) < 0) {
        fprintf(stderr, "%s: %s\n", bpf_path, strerror(errno));
        goto exit_bpf;
    }

    // schliesslich physikalisches geraet mit bpf-geraet verheiraten
    struct ifreq ifr;
    strlcpy(ifr.ifr_name, interface_name, IFNAMSIZ);
    if (ioctl(bpf, BIOCSETIF, &ifr) == -1) {
        fprintf(stderr, "%s: %s\n", interface_name, strerror(errno));
        goto exit_bpf;
    }

    // frame puffer
    char *frame = (char *)malloc(FRAME_SIZE);
    if (frame == NULL) {
        fprintf(stderr, "%s\n", strerror(errno));
        goto exit_frame;
    }

    // nibbles zu hex und im frame speichern
    int n = 0;
    char nib;
    while(fread(&nib, 1, 1, stdin) != 0 && n < FRAME_SIZE) {
        static int k = 0; // ob gerade oder ungerade
        static char msn; // msn zwischenspeichern, bis lsn gelesen

        if (nib >= '0' && nib <= '9')		{ nib -= 48; }
        else if (nib >= 'a' && nib <= 'f')	{ nib -= 87; }
        else if (nib >= 'A' && nib <= 'F')	{ nib -= 55; }
        else	                            { continue;  }

        if (k%2 == 0)
            msn = nib << 4;
        else
            frame[n++] = msn | nib;

        ++k; // valides nibble verarbeitet
    }

//for (int i=0; i<n; ++i)
//    printf("%02x ", frame[i] & 0xff);
//printf("\n");

    // frame schreiben
    if (write(bpf, frame, n) != n)
        fprintf(stderr, "%s: %s\n", interface_name, strerror(errno));

    status = EXIT_SUCCESS; // wenn bis hierhin gekommen, dann ok

exit_frame:
    free(frame);
exit_bpf:
    close(bpf);
exit:
    exit(status);
}
