# linkcat

ist ein Tool, um rohe Daten auf dem Netzwerkinterface auszugeben. Damit kann man z.B. händisch ARP-Anfragen machen oder Pakete mit falschen Headern senden.

## Anforderungen

* OS mit `/dev/bpfN` (z.B. MacOS X oder OpenBSD)
* root-Rechte bei Ausführung 
